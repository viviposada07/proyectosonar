import json
import awswrangler as wr
import pandas as pd
import boto3
import pytz
from datetime import datetime, timedelta

s3 = boto3.client('s3')

def get_now():
    """
    Crea un date time object. Utiliza pytz para obtener el timezone
    :return: retorna date time object con formato %Y-%m-%d %H:%M:%S
    """
    tz = pytz.timezone('America/Santiago')
    dateTimeObj = datetime.now(tz)
    return dateTimeObj.strftime("%Y-%m-%d %H:%M:%S")
    
def insert_data(data, path, wr_mode=None, partition_cols = None, glue_db=None, glue_table=None, catalog_id=None):
    """
    Inserta datos en S3 segun endpoint. El path proviene de job parameters
    :param data: Dataframe a guardar en S3
    :param path: Path en donde se guarda la informacion en S3
    :param wr_mode: Modo de escritura en S3. Por defecto overwrite_partitions
    :param glue_db: Base de datos de Glue en donde se almacena la informacion
    :param glue_table: Tabla de la base de datos en la cual se almacena la informacion
    :param catalog_id: Numero de la cuenta en donde se almacena la informacion
    :param partition_cols: Lista con strings que corresponden a las particiones
    :return: None
    """
    try:
        write_params = {
            "df": data,
            "path": path,
            "dataset": True,
            "index": False,
            "mode": wr_mode,
            "compression": 'gzip',
            "schema_evolution": True,
            "s3_additional_kwargs": {'ACL': 'bucket-owner-full-control'},
            "database": glue_db,
            "table": glue_table,
            "catalog_id": catalog_id,
            "partition_cols": partition_cols
        }
        
        wr.s3.to_parquet(**write_params)
        print('Done')
    except Exception as e:
        raise Exception(
            "Insertando datos en {} error: {}".format(path, str(e)))

def json_to_df(url):
    """
    Busca datos en json y se convierten en df y se agregan las partitions cols
    :param path: Path en donde se guarda la informacion en S3
    :return: dataframe
    """
    
    try:
        df = wr.s3.read_json(path=url)
        print('dtypes 1', df.dtypes)
        df = df.astype(str)
        print('dtypes 2', df.dtypes)
        df['year'] = get_now()[:4]
        df['month'] = get_now()[5:7]
        df['day'] = get_now()[8:10]
        return df
        
    except Exception as e:
        raise e

#def lambda_handler(event, context):
if __name__ == "__main__":  

    source_bucket = 'cencosud.desa.corp-bi.reg.raw'
    source_folder = 'sourcesystem/gitlab-scm.cencosud.net'
    target_bucket = 'cencosud.desa.corp-bi.reg.raw'
    target_folder = 'transient/gitlab-scm.cencosud.net'
    endpoints = ['groups', 'issues', 'labels', 'milestones']
    partition_cols = ['year', 'month', 'day']
    #partition_values = ['2022','08','19']
    fecha_now = get_now()
    partition_values = [str(fecha_now[:4]),str(fecha_now[5:7]),str(fecha_now[8:10])]

    for endpoint in endpoints:
        try:
            path = f's3://{source_bucket}/{source_folder}/{endpoint}/{partition_cols[0]}={partition_values[0]}/{partition_cols[1]}={partition_values[1]}/{partition_cols[2]}={partition_values[2]}/'
            df = json_to_df(path)
            insert_data(df,
                        f's3://{target_bucket}/{target_folder}/{endpoint}',
                        "overwrite_partitions",
                        partition_cols)
        except Exception as e:
            raise Exception(
                "Insertando datos en {} error: {}".format(path, str(e)))

