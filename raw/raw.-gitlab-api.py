import json
import awswrangler as wr
import pandas as pd
import boto3
import pytz
from datetime import datetime, timedelta

s3 = boto3.client('s3')

def get_now():
    """
    Crea un date time object. Utiliza pytz para obtener el timezone
    :return: retorna date time object con formato %Y-%m-%d %H:%M:%S
    """
    tz = pytz.timezone('America/Santiago')
    dateTimeObj = datetime.now(tz)
    return dateTimeObj.strftime("%Y-%m-%d %H:%M:%S")
    
def read_parquet_data(source_path, partition_cols = None, partition_values = None):
    """
    Lee archivos parquet con la condicion de particion si es que viene incluida.
    Los valores de partition_cols y partition_values deben ir en el orden correcto
    Ejemplo:
    s3://parauco-analytics-datalake-corp-dev-ingestion/sourcesystem/int/opscentroscomerciales/cms/access_token/year=2021/month=10/day=26/
    source_path = 's3://parauco-analytics-datalake-corp-dev-ingestion/sourcesystem/int/opscentroscomerciales/cms/access_token/'
    partition_cols = ['year', 'month', 'day']
    partition_values = ['2021', '10', '26']
    :param source_path: path del archivo a leer
    :param partition_cols: fecha de busqueda
    :param partition_values: valores de la particion que se quieren buscar
    :return: dataframe de la informacion leida del parquet
    """
    if partition_cols == None:
        print('partition cols none')
        print(f"iniciando importacion: {source_path}")
        df = wr.s3.read_parquet(path=source_path)
    else:
        print('partition cols none')
        print(f"iniciando importacion: {source_path}")
        for i in range(len(partition_cols)):
            source_path = source_path + partition_cols[i] + '=' + partition_values[i] +'/'
        print(source_path)
        df = wr.s3.read_parquet(path=source_path)
        for i in range(len(partition_cols)):
            df[partition_cols[i]]=partition_values[i]

    return df
    
def insert_data(data, path, wr_mode=None, partition_cols = None, glue_db=None, glue_table=None, catalog_id=None):
    """
    Inserta datos en S3 segun endpoint. El path proviene de job parameters
    :param data: Dataframe a guardar en S3
    :param path: Path en donde se guarda la informacion en S3
    :param wr_mode: Modo de escritura en S3. Por defecto overwrite_partitions
    :param glue_db: Base de datos de Glue en donde se almacena la informacion
    :param glue_table: Tabla de la base de datos en la cual se almacena la informacion
    :param catalog_id: Numero de la cuenta en donde se almacena la informacion
    :param partition_cols: Lista con strings que corresponden a las particiones
    :return: None
    """
    try:
        print('path', path)
        write_params = {
            "df": data,
            "path": path,
            "dataset": True,
            "index": False,
            "mode": wr_mode,
            "compression": 'gzip',
            "schema_evolution": True,
            "s3_additional_kwargs": {'ACL': 'bucket-owner-full-control'},
            "database": glue_db,
            "table": glue_table,
            "catalog_id": catalog_id,
            "partition_cols": partition_cols
        }
        
        wr.s3.to_parquet(**write_params)
        print('Done')
    except Exception as e:
        raise Exception(
            "Insertando datos en {} error: {}".format(path, str(e)))
        
#def lambda_handler(event, context):
if __name__ == "__main__":  

    source_bucket = 'cencosud.desa.corp-bi.reg.raw'
    source_folder = 'transient/gitlab-scm.cencosud.net'
    target_bucket = 'cencosud.desa.corp-bi.reg.raw'
    target_folder = 'raw/gitlab-scm.cencosud.net'
    endpoints = ['groups', 'issues', 'labels', 'milestones']
    partition_cols = ['year', 'month', 'day']
    #partition_values = ['2022','08','19']
    fecha_now = get_now()
    partition_values = [str(fecha_now[:4]),str(fecha_now[5:7]),str(fecha_now[8:10])]
    

    for endpoint in endpoints:
        try:
            print('endpoint processing: ',endpoint)
            path_ = 's3://'+source_bucket+'/'+source_folder+'/'+endpoint+'/'
            df = read_parquet_data(path_,
                                    partition_cols,
                                    partition_values
                                    )         
            #df = df.astype(str)
            print('dtypes', df.dtypes)
            path = f's3://{source_bucket}/{source_folder}/{endpoint}/{partition_cols[0]}={partition_values[0]}/{partition_cols[1]}={partition_values[1]}/{partition_cols[2]}={partition_values[2]}/'
            insert_data(df, 's3://'+target_bucket+'/'+target_folder+'/'+endpoint,
                        'overwrite_partitions',
                        partition_cols,
                        'cenco-capa1-raw-test',
                        'gitlab_scm_'+endpoint
                        )                     
        except Exception as e:
            raise Exception(
                "Insertando datos en {} error: {}".format(path, str(e)))    

